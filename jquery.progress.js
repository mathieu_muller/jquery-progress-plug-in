// The semi-colon before function invocation is a safety net against concatenated
// scripts and/or other plugins which may not be closed properly.
; ( function ( $, window, document, undefined ) 
{
	// undefined is used here as the undefined global variable in ECMAScript 3 is
	// mutable (ie. it can be changed by someone else). undefined isn't really being
	// passed in so we can ensure the value of it is truly undefined. In ES5, undefined
	// can no longer be modified.

	// window and document are passed through as local variable rather than global
	// as this (slightly) quickens the resolution process and can be more efficiently
	// minified (especially when both are regularly referenced in your plugin).

	// Create the defaults once
	var plugin_name = "progress";

	var defaults = 
	{
		frequency: 1000
	};

	// List of progress bars to update
	var items = [];

	// The actual plugin constructor
	function plugin( selector, element, options ) 
	{
		this.element = element;
		this.selector = selector;

		// jQuery has an extend method which merges the contents of two or
		// more objects, storing the result in the first object. The first object
		// is generally empty as we don't want to alter the default options for
		// future instances of the plugin
		this.options = $.extend( {}, defaults, options );

		this._defaults = defaults;
		this._name = plugin_name;

		this.init();
	};

	plugin.prototype = 
	{
		init: function() 
		{
			// Add the current item to the list of items to update
			items.push( $( this.element ) );
		}
	};

	// A really lightweight plugin wrapper around the constructor,
	// preventing against multiple instantiations
	$.fn[ plugin_name ] = function ( options ) 
	{
		// Trampoline the selector to each element
		var selector = this.selector;
		
		return( this.each( function () 
		{
			if( !$.data( this, "plugin_" + plugin_name ) ) 
			{
				$.data( this, "plugin_" + plugin_name, new plugin( selector, this, options ) );
			}
		} ) );
	};
	
	// Static initializer methods goes here
	setInterval( function()
	{
		// Save the time for progress
		var time = new Date().getTime() / 1000;
		
		$( items ).each( function( index, $this )
		{
			if( !jQuery.contains( document, $this[ 0 ] ) )
			{
				// Remove the object from the items array as it won't be updated anymore
				items.splice( index, 1 );
			
				// Continue the next iteration
				return true;
			}
			
			// Fetch the start and end params
			var start = parseInt( $this.attr( 'data-progress-start' ), 10 );
			var end   = parseInt( $this.attr( 'data-progress-end' ), 10 );
			
			var untrimmed_progress = 0;
			var progress           = 0;
			
			// If there is any kind of progress duration, compute the progression
			if( !isNaN( start ) && !isNaN( end ) && start != end )
			{
				// Fetch the progression value
				untrimmed_progress = ( ( time - start ) / ( end - start ) * 100 );
				progress           = Math.min( 100, Math.max( 0, untrimmed_progress ) );
			}
			
			// If the start time is the same as the end time, consider the progression as done
			else
			{
				untrimmed_progress = 100;
				progress           = 100;
			}
			
			// Trigger a progress bar initialization event
			if( isNaN( $this.value ) )
			{
				$this.trigger( 'progress.init', 0 );
			}
			
			if( !$this.value )
			{
				$this.value = 0;
			}
			
			if( untrimmed_progress > $this.value )
			{
				// Updates the progress bar
				$this.find( '> .bar' ).css( 'width', progress + '%' );
				
				// Change the color of the bar when finished
				$this
					.toggleClass( 'start', progress == 0 )
					.toggleClass( 'progressing', progress > 0 && progress < 100 )
					.toggleClass( 'end', progress == 100 );
				
				// Trigger a progress bar start event
				if( $this.value == 0 && untrimmed_progress >= 0 )
				{
					$this.trigger( 'progress.start', [ progress ] );
				}
				
				// Trigger a progress bar progress event
				if( untrimmed_progress >= 0 && untrimmed_progress <= 100 )
				{
					$this.trigger( 'progress.progress', [ progress ] );
				}
				
				// Trigger a progress bar end event
				if( $this.value < 100 && untrimmed_progress >= 100 )
				{
					$this.trigger( 'progress.end', [ progress ] );
				}
				
				$this.value = untrimmed_progress;
			}
		} );
		
	}, 20 );
	
} ) ( jQuery, window, document );